resource "gitlab_project" "code_style" {
  name                   = "Coding Style and Guidelines"
  description            = "Coding Style and Guidelines"
  default_branch         = var.default_branch_name_091322
  namespace_id           = data.gitlab_group.arpanrec.id
  path                   = "code_style"
  visibility_level       = "public"
  shared_runners_enabled = true
  topics                 = [var.project_topic_controlled_by_master]
}
