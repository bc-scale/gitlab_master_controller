resource "gitlab_project" "homer" {
  name                   = "Homer - Homepage"
  description            = "Home Page"
  default_branch         = var.default_branch_name_091322
  namespace_id           = data.gitlab_group.arpanrec.id
  path                   = "homer"
  visibility_level       = "public"
  shared_runners_enabled = true
  topics                 = [var.project_topic_controlled_by_master]
}
