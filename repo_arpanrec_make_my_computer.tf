resource "gitlab_project" "make_my_computer" {
  name                   = "Make My Computer"
  description            = "Automated script for os installation"
  default_branch         = var.default_branch_name_091322
  namespace_id           = data.gitlab_group.arpanrec.id
  path                   = "make_my_computer"
  visibility_level       = "public"
  shared_runners_enabled = true
  topics                 = [var.project_topic_controlled_by_master]
}
