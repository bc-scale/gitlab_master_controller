data "gitlab_group" "arpanrec" {
  full_path = "arpanrec"
}

data "vault_kv_secret_v2" "gitlab_creds" {
  mount = "prerequisite"
  name  = "gitlab"
}

data "http" "global_config" {
  url = var.GLOBAL_CONFIG_ENDPOINT

  # Optional request headers
  request_headers = {
    Content-Type = "application/json"
  }
}

locals {
  # VAULT_DOMAIN_NAME    = element(split(":", element(split("/", var.VAULT_ADDRESS), 2)), 0)
  GLOBAL_CONFIG                = jsondecode(data.http.global_config.response_body)
  GLOBAL_CONFIG_VAULT_FQDN     = local.GLOBAL_CONFIG.VAULT.FQDN
  GLOBAL_CONFIG_VAULT_PORT     = local.GLOBAL_CONFIG.VAULT.PORT
  GLOBAL_CONFIG_VAULT_PROTOCOL = local.GLOBAL_CONFIG.VAULT.PROTOCOL
  VAULT_ADDR                   = "${local.GLOBAL_CONFIG_VAULT_PROTOCOL}://${local.GLOBAL_CONFIG_VAULT_FQDN}:${local.GLOBAL_CONFIG_VAULT_PORT}"
  GL_PROD_API_KEY              = data.vault_kv_secret_v2.gitlab_creds.data["GL_PROD_API_KEY"]
}
