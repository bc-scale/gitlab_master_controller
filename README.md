# GitLab Master Control

Manage [GitLab](https://gitlab.com/arpanrec) projects and groups using [terraform](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)

** [Issue with GitLab Group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group)

On GitLab SaaS, you must use the GitLab UI to create groups without a parent group. You cannot use this provider nor the API to do this.
Add terraform resource and before applying run group import statements. `terraform import <resource> <id>`

```hcl
terraform import gitlab_group.example 123456
```

## Terraform Root Directory: `./terraform`

## Required Variables / GitLab Setup

Add the variables in GitLab CI/CD Pipeline variable for project

- GLOBAL_CONFIG_ENDPOINT=`https://raw.githubusercontent.com/arpanrecme/dotfiles/main/.config/global.json`
- VAULT_APPROLE_AUTH_MOUNT=`"<Approle Mount>"`
- VAULT_APPROLE_ID=`"<Vault approle id>"`
- VAULT_APPROLE_SECRET_ID=`"<Vault approle id secret>"`
- ROOT_CA_CERTIFICATE=`<Root CA Certificate>`
- VAULT_CLIENT_PRIVATE_KEY=`<Vault Client Private Key for mutual TLS>`
- VAULT_CLIENT_CHAIN_CERTIFICATE=`<Vault Client Certificate for mutual TLS>`

Terraform State File Name: `gitlab_master_controller`

## Local Development

### Work Directory

- `terraform`

### Prepare: Init from [GitLab Docs](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#access-the-state-from-your-local-machine)

Example

```shell
export GITLAB_ACCESS_TOKEN="<gitlab-personal-access-token>"
export PROJECT_ID="<gitlab-project-id>"
export TF_USERNAME="<gitlab-username>"
export TF_STATE_NAME="gitlab_master_controller"
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock" \
    -backend-config="username=${TF_USERNAME}" \
    -backend-config="password=${GITLAB_ACCESS_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

### Make changes and plan the changes with `terraform plan`

```shell
terraform plan -input=false -out="./tfplan"
```

### Apply the changes with `terraform apply`

```shell
terraform apply "./tfplan"
```

## License

`MIT`
