resource "vault_pki_secret_backend_cert" "vault_client_certificate" {
  backend     = "pki"
  name        = "vault_client_certificate"
  common_name = local.GLOBAL_CONFIG_VAULT_FQDN
}

resource "gitlab_group_variable" "global_config_endpoint" {
  group         = data.gitlab_group.arpanrec.id
  key           = "GLOBAL_CONFIG_ENDPOINT"
  value         = var.GLOBAL_CONFIG_ENDPOINT
  protected     = false
  masked        = true
  variable_type = "env_var"
}

resource "gitlab_group_variable" "root_ca_certificate" {
  group         = data.gitlab_group.arpanrec.id
  key           = "ROOT_CA_CERTIFICATE"
  value         = file(var.LOCAL_FILE_ROOT_CA_CERTIFICATE)
  protected     = false
  masked        = false
  variable_type = "env_var"
}

resource "gitlab_group_variable" "vault_client_chain_certificate" {
  group = data.gitlab_group.arpanrec.id
  key   = "VAULT_CLIENT_CHAIN_CERTIFICATE"
  value = format(
    "%s\n%s",
    vault_pki_secret_backend_cert.vault_client_certificate.certificate,
    vault_pki_secret_backend_cert.vault_client_certificate.ca_chain
  )
  protected     = false
  masked        = false
  variable_type = "env_var"
}

resource "gitlab_group_variable" "vault_client_private_key" {
  group         = data.gitlab_group.arpanrec.id
  key           = "VAULT_CLIENT_PRIVATE_KEY"
  value         = vault_pki_secret_backend_cert.vault_client_certificate.private_key
  protected     = false
  masked        = false
  variable_type = "env_var"
}
