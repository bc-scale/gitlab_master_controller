variable "default_branch_name_091322" {
  type      = string
  default   = "main"
  sensitive = false
}

variable "project_topic_controlled_by_master" {
  type      = string
  default   = "controlled_by_master"
  sensitive = false
}

variable "GLOBAL_CONFIG_ENDPOINT" {
  type      = string
  default   = "https://raw.githubusercontent.com/arpanrecme/dotfiles/main/.config/global.json"
  sensitive = false
}

variable "LOCAL_FILE_ROOT_CA_CERTIFICATE" {
  type      = string
  default   = "root_ca_certificate.pem"
  sensitive = false
  validation {
    condition     = length(var.LOCAL_FILE_ROOT_CA_CERTIFICATE) > 1
    error_message = "Missing root certificate path"
  }
}
