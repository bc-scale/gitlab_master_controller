resource "gitlab_project" "backup_project" {
  name                   = "Backup Project"
  description            = "Cluster Backup"
  default_branch         = var.default_branch_name_091322
  namespace_id           = data.gitlab_group.arpanrec.id
  path                   = "backup_project"
  visibility_level       = "public"
  shared_runners_enabled = true
  topics                 = [var.project_topic_controlled_by_master]
}

resource "gitlab_branch" "backup_project_feature_inprogress" {
  name    = "feature/inprogress"
  ref     = var.default_branch_name_091322
  project = gitlab_project.backup_project.id
}
